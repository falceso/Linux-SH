#!/bin/bash

bungeecord="/srv/daemon-data/9909f635-2d3b-402b-aaef-e5ce442438a3"
exclude_server="/srv/daemon-data/9bc58eee-f8d4-472c-bfa6-e01b961fe151"
server_options()         {
echo "Download a file to [1] all servers, [2] bungeecord, or [3] regular servers only."
echo "Type your choice"
read choice1
    case $choice1 in
        1 ) server_option=1
            echo "You have selected all servers."
            ;;
        2 ) server_option=2
            echo "You have selected bungeecord only."
            ;;
        3 ) server_option=3
            echo "You have selected regular servers."
            ;;
        * ) echo "You did not enter a a valid selection."
            server_options
    esac
}

download_to() {

echo "Provide the download link."
read dl
echo "What is the file name? (Include file extensions)"
read fn
wget $dl -O $fn
sleep 3

if [ "$server_option" = "1" ]; then
echo "Rsyncing to all servers."
for i in `ls /srv/daemon-data/`
do
echo "Processing folder $i"
rsync -n -avz --exclude $exclude_server ./$fn /srv/daemon-data/$i/plugins/$fn
done
elif [ "$server_option" = "2" ]; then
echo "Rsyncing to bungeecord."
rsync -n -avz ./$fn $bungeecord/plugins/$fn
elif [ "$server_option" = "3" ]; then
echo "Rsyncing to regular servers."
rsync -n -avz --exclude $exclude_server --exclude $bungeecord ./$fn /srv/daemon-data/*/plugins/$fn
fi
}

clean_up() {
echo "Would you like to delete the downloaded file?"
read delete
case $delete in
     yes|y|Y )
              rm -rf $fn
              echo "File Deleted ($fn)"
               ;;
     No|n|N ) echo "File not deleted."
              ;;
     * ) echo "You did not enter a a valid selection."
         clean_up

    esac
}

server_options
download_to
clean_up
echo "End of Script."
