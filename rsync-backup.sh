#!/bin/bash
mkdir /backups/$(date '+%a_%b-%d-%Y')/
mkdir /backups/$(date '+%a_%b-%d-%Y')/$(date '+%H%p')

cd /srv/daemon-data

for i in *
do
echo "Processing folder $i";
[ -d "$i" ] && tar -jcvf "$i.tar.bz2" "$i"
echo "SUCCESS! Created ${i%/}.tar.bz2"
sleep 3
rsync -avzh --remove-source-files $i.tar.bz2 /backups/$(date '+%a_%b-%d-%Y')/$(date '+%H%p')/
rsync --progress --remove-source-files -e 'ssh -p23 -i /root/.ssh/(LOCATION OF SSH KEY)' --recursive /backups/$(date '+%a_%b-%d-%Y') USER@SERVERIP:DIRECTORY
done
echo "SUCCESSFULLY COMPLETE BACKUP!"
